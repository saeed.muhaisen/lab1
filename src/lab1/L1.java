package lab1;

public class L1 {
	double x;
	double y;
	double radius;
	
	public double getX(){
	return x;
	}
	public void setX(double xd) {
	 x=xd;
	 
	}
	public double getY() {
	return y;
	}
	public void setY(double yd) {
	y=yd;
	} 
	public double getR() {
		return radius;
	}
	public void setR(double R) {
		radius=R;
	}
	public double getArea() {
		return Math.PI* Math.pow(radius, 2);
	}
	public void translate(double dx, double dy) {
		y=y+dy;
		x=x+dx;
		
	}
	public double scale(double scaler) {
		return radius*scaler;
	}
}

	
